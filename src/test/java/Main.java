import java.io.IOException;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import br.edu.ifam.hateoas.LibField;
import gibi.Department;

public class Main {

    private static final Gson gson = new GsonBuilder().setPrettyPrinting().create();

    private static void requestLinkHateoas() throws IOException {

        LibField<Department> lf = new LibField<>(Department.class, "http://localhost:8080/api/v1/departments/1");
        Department dp= new Department();
        //dp.setName("BSZ novo");

        System.out.println("Departamento" + gson.toJson(lf.getObject(dp, Department.class).getLinks().getDepartment()));

        //lf.getObject().getLinks().getDepartment().doPost(dp);

//        lf.getObject().getLinks().getDepartment().doPost(dp);
       // lf.getObject().getLinks().getDepartment().doPut(1L, dp);
//      System.out.println("Department ->" + gson.toJson(lf.getObject(dp, Department.class)));
    }

    public static void main(String[] args) throws IOException, NoSuchFieldException, SecurityException {

        requestLinkHateoas();
    }
}
