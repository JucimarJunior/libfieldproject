package gibi;

import br.edu.ifam.hateoas.HateoasObject;

public class Department extends HateoasObject<DepartmentLinks, Object> {
    private String id;
    private String name;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
