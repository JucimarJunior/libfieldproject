package gibi;

import br.edu.ifam.hateoas.HateoasObject;

import java.util.List;
import java.util.Map;

//Antes era:
//public class Gibi extends HateoasObject<GibiLinks, Object> {

public class Gibi extends HateoasObject<GibiLinks, Object> {
  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  private Long id;

  private String titulo;
  private String nome;
  private String numero;
  private String editora;

  public String getTitulo() {
    return titulo;
  }

  public void setTitulo(String titulo) {
    this.titulo = titulo;
  }

  public String getNome() {return nome;}

  public String setNome(String nome) {
    this.nome = nome;
    return nome;
  }

  public String getNumero() {
    return numero;
  }

  public void setNumero(String numero) {
    this.numero = numero;
  }

  public String getEditora() {
    return editora;
  }

  public void setEditora(String editora) {
    this.editora = editora;
  }
}
