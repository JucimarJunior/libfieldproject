package gibi;

import br.edu.ifam.link.LinkObject;

public class GibiLinks {
  private LinkObject self;
  private LinkObject gibi;

  public LinkObject getSelf() {
    return self;
  }

  public void setSelf(LinkObject self) {
    this.self = self;
  }

  public LinkObject getGibi() {
    return gibi;
  }

  public void setGibi(LinkObject gibi) {
    this.gibi = gibi;
  }
}
