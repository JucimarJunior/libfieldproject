package gibi;

import br.edu.ifam.link.LinkObject;
import br.edu.ifam.methodLinks.MethodLinks;

import java.util.ArrayList;

public class DepartmentLinks {
    private MethodLinks self;
    private MethodLinks department;

    public MethodLinks getSelf() {
        return self;
    }

    public void setSelf(MethodLinks self) {
        this.self = self;
    }

    public MethodLinks getDepartment() {
        return department;
    }

    public void setDepartment(MethodLinks department) {
        this.department = department;
    }
}
