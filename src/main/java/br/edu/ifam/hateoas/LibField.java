package br.edu.ifam.hateoas;

import java.io.IOException;
import java.util.Map;

import br.edu.ifam.gson.GsonUtil;
import br.edu.ifam.link.LinkObject;
import com.google.gson.*;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.RequestBody;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.*;
import retrofit2.converter.gson.GsonConverterFactory;
//TODO: Remover da versão final(Utilizado somente para exemplos de consulta)
public class LibField<H> extends LinkObject {
    private Service service;
    private Class<H> typeOf;

    public LibField(Class<H> typeOf, String url) {
        this.typeOf = typeOf;
        this.setHref(this.getResourcePath(url));

        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();

        this.service = new Retrofit.Builder()
                .baseUrl(getBaseUrl(url))
                .addConverterFactory(GsonConverterFactory.create())
                .client(client)
                .build()
                .create(Service.class);
    }

    private String getBaseUrl(String url) {
        return getUrl(url, true);
    }

    private String getResourcePath(String url) {
        return getUrl(url, false);
    }

    private String getUrl(String url, boolean baseUrl) {
        int protocolEnd = url.indexOf("://") + 3;
        int pathStart = url.substring(protocolEnd).indexOf("/");
        String newUrl;

        if (baseUrl) {
            newUrl = url.substring(0, pathStart + protocolEnd);
        } else {
            newUrl = url.substring(pathStart + protocolEnd);
        }

        return newUrl;
    }

    public H getObject(Object query, Class<H> responseType) throws IOException {
        Map<String, Object> queryMap = GsonUtil.getMap(query);
        Response<JsonElement> response = service.get(this.getHref(), queryMap).execute();
        if (response.isSuccessful()) {
            JsonElement jsonElement = response.body();
            System.out.println("Sucesso " + response.code());
            return GsonUtil.<H>convert(jsonElement, responseType);
        } else {
            System.out.println("Erro na requisição " + response.code());
            throw new IOException("Erro na requisição: " + response.code() + " - " + response.message());
        }
    }

    public H getObject() throws IOException {
        return GsonUtil.<H>convert(service.get(this.getHref()).execute().body(), typeOf);
    }

    public H getObject(int id) throws IOException {
        return GsonUtil.<H>convert(service.get(this.getHref().concat("/" + id)).execute().body(), typeOf);
    }

    public <P> P post(Object object, Class<P> type) throws IOException {
        RequestBody requestBody = RequestBody.create(GsonUtil.toJson(object), MediaType.parse("application/json; charset=utf-8"));
        Response<JsonElement> response = service.post(this.getHref(), requestBody).execute();
        if(response.isSuccessful()) {
            JsonElement jsonElement = response.body();
            System.out.println("Sucesso " + response.code());
            return GsonUtil.convert(jsonElement, type);
        } else {
            System.out.println("Erro na requisição " + response.code());
            throw new IOException("Erro na requisição: " + response.code() + " - " + response.message());
        }
    }

    public <T> T put(Object object, Class<T> type) throws IOException {
        Response<JsonElement> response = service.put(this.getHref(), object).execute();

        if (response.isSuccessful()) {
            JsonElement jsonElement = response.body();
            System.out.println("Sucesso " + response.code());
            return GsonUtil.convert(jsonElement, type);
        } else {
            System.out.println("Erro na requisição " + response.code());
            throw new IOException("Erro na requisição: " + response.code() + " - " + response.message());
        }
    }

    public <C> C patch(Object object, Class<C> type) throws IOException {
        Response<JsonElement> response = service.patch(this.getHref(), object).execute();
        if (response.isSuccessful()) {
            JsonElement jsonElement = response.body();
            System.out.println("Sucesso " + response.code());
            return GsonUtil.convert(jsonElement, type);
        } else {
            System.out.println("Erro na requisição " + response.code());
            throw new IOException("Erro na requisição: " + response.code() + " - " + response.message());
        }
    }

    public H delete(int id) throws IOException {
        Response<JsonElement> response = service.delete(this.getHref().concat("/" + id)).execute();
        if (response.isSuccessful()) {
            JsonElement jsonElement = response.body();
            System.out.println("Sucesso " + response.code());
            return GsonUtil.convert(jsonElement, typeOf);
        } else {
            System.out.println("Erro na requisição " + response.code());
            throw new IOException("Erro na requisição: " + response.code() + " - " + response.message());
        }
    }
}
