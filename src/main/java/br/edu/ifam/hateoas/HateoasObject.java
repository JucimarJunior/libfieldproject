package br.edu.ifam.hateoas;

import com.google.gson.annotations.SerializedName;

public class HateoasObject<LinkResponse, EmbeddedResponse> {
  @SerializedName("_links")
//  @SerializedName("_links.self")
  private LinkResponse links;

  @SerializedName("_embedded")
  private EmbeddedResponse embedded;

  public LinkResponse getLinks() {
    return links;
  }

  public EmbeddedResponse getEmbedded() {
    return embedded;
  }
}
