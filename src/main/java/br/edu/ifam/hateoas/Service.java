package br.edu.ifam.hateoas;

import java.util.Map;

import com.google.gson.JsonElement;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.*;

public interface Service {

    @GET
    public Call<JsonElement> get(@Url String url, @QueryMap Map<String, Object> query);

    @GET
    public Call<JsonElement> get(@Url String url);

    @Headers({"Accept: application/json"})
    @POST
    public Call<JsonElement> post(@Url String url, @Body Object object);

    @Headers({"Accept: application/json"})
    @POST
    public Call<JsonElement> post(@Url String url, @Body RequestBody object);

    @PUT
    public Call<JsonElement> put(@Url String url, @Body Object object);

    @PATCH
    public Call<JsonElement> patch(@Url String url, @Body Object object);

    @DELETE
    public Call<JsonElement> delete(@Url String url);

}
