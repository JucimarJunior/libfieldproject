package br.edu.ifam.gson;

import java.util.Map;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.reflect.TypeToken;

public class GsonUtil {
    private static Gson gson = new Gson();
    public static <E> E convert(JsonElement element, Class<E> e) {
    return gson.fromJson(element, e);
  }

  public static String toJson(Object object) {
        return gson.toJson(object);
  }

  public static Map<String, Object> getMap(Object query) {
    return gson.fromJson(gson.toJson(query), new TypeToken<Map<String, Object>>() {}.getType());
  }
}
