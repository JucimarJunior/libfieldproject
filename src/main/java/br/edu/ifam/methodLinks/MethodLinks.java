package br.edu.ifam.methodLinks;

import br.edu.ifam.hateoas.Service;
import br.edu.ifam.link.LinkObject;
import com.google.gson.JsonElement;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import java.io.IOException;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public final class MethodLinks extends ArrayList<LinkObject> {
    public void doPut(Long id, Object object) throws IOException {
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();

        String href = this.stream().filter(linkObject -> linkObject.getType().equals("PUT")).findFirst().get().getHref();
        href = replace(href, id.toString())+"/";

        Service service = new Retrofit.Builder()
                .baseUrl(href)
                .addConverterFactory(GsonConverterFactory.create())
                .client(client)
                .build()
                .create(Service.class);
        service.put(href, object).execute();

        Response<JsonElement> response = service.put(href, object).execute();

        if (response.isSuccessful()) {
            System.out.println("Sucesso PUT: "+ response.code());
        } else {
            int statusCode = response.code();
            if (statusCode >= 400 && statusCode < 500) {
                System.out.println("Erro cliente: " + statusCode + " - " + response.message());
            } else if (statusCode >= 500) {
                System.out.println("Erro do servidor: " + statusCode + " - " + response.message());
            } else {
                System.out.println("Erro inesperado: " + statusCode + " - " + response.message());
            }
        }
    }

    public void doPost(Object object) throws IOException {
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();

        String href = this.stream().filter(linkObject -> linkObject.getType().equals("POST")).findFirst().get().getHref();
        href += "/";

        Service service = new Retrofit.Builder()
                .baseUrl(href)
                .addConverterFactory(GsonConverterFactory.create())
                .client(client)
                .build()
                .create(Service.class);
        service.post(href, object).execute();

        Response<JsonElement> response = service.post(href, object).execute();

        if (response.isSuccessful()) {
            System.out.println("Sucesso POST: " + response.code());
        } else {
            int statusCode = response.code();
            if (statusCode >= 400 && statusCode < 500) {
                System.out.println("Erro do cliente: " + statusCode + " - " + response.message());
            } else if (statusCode >= 500) {
                System.out.println("Erro do servidor: " + statusCode + " - " + response.message());
            } else {
                System.out.println("Erro inesperado: " + statusCode + " - " + response.message());
            }
        }
    }

    public Object doGet() throws IOException {
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();

        String href = this.stream().filter(linkObject -> linkObject.getType().equals("GET")).findFirst().get().getHref();
        href += "/";

        Service service = new Retrofit.Builder()
                .baseUrl(href)
                .addConverterFactory(GsonConverterFactory.create())
                .client(client)
                .build()
                .create(Service.class);
        return service.get(href).execute().body();
    }

    public Object doDelete() throws IOException {
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();

        String href = this.stream().filter(linkObject -> linkObject.getType().equals("Delete")).findFirst().get().getHref();
        href += "/";

        Service service = new Retrofit.Builder()
                .baseUrl(href)
                .addConverterFactory(GsonConverterFactory.create())
                .client(client)
                .build()
                .create(Service.class);
        return service.delete(href).execute().body();
    }

    public static String replace(String url, String replacement) {
        Pattern pattern = Pattern.compile("\\{[^/]+\\}");
        Matcher matcher = pattern.matcher(url);

        if (matcher.find()) {
            return matcher.replaceFirst(replacement);
        }
        return url;
    }
}
