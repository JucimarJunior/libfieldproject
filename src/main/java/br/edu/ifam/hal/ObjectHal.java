package br.edu.ifam.hal;
public class ObjectHal<L, E> {
    private L _links;
    private E _embedded;

    public L get_links() {
        return _links;
    }

    public void set_links(L _links) {
        this._links = _links;
    }

    public E get_embedded() {
        return _embedded;
    }

    public void set_embedded(E _embedded) {
        this._embedded = _embedded;
    }

}
