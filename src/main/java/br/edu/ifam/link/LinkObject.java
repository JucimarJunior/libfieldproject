package br.edu.ifam.link;

public class LinkObject {
    private String href;
    private String rel;
    private String type;
    private String deprecation;
    private Boolean templated;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getDeprecation() {
        return deprecation;
    }

    public void setDeprecation(String deprecation) {
        this.deprecation = deprecation;
    }

    public Boolean getTemplated() {
        return templated;
    }

    public void setTemplated(Boolean templated) {
        this.templated = templated;
    }

    public String getRel() {return rel;}

    public void setRel(String rel) { this.rel = rel; }

    public String getHref() {
        return href;
    }

    public void setHref(String href) {
        this.href = href;
    }
}
