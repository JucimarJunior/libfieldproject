## Biblioteca LibField

## Descrição
O projeto é uma biblioteca para usuários desenvolvedores de aplicações android e fornece meios mais rápidos para consumir APIs REST que tenham conteúdo HATEOAS, no formato HAL, para uso em aplicações Android

## Instalação
Para utilizar a lib em sua aplicação você precisará, antes de tudo, baixar o arquivo JAR disponível nas releases desse projeto, de preferência a última versão. Após baixado, você terá que
criar uma pasta dentro do seu projeto, por exemplo, "libs" e colocar o arquivo JAR dentro dessa pasta. Após realizar este passo, no seu Android Studio vá em Projects Structure --> Modules --> Dependencies e clicar na seta de '+' e
escolher a opção "JARs or Directories" e selecionar o arquivo JAR que está dentro da pasta que você criou no primeiro passo.
Para finalizar, você precisa implementar a lib no build gradle do módulo de app, o caminho que você copiou o JAR baixado. dessa forma:

```implementation files('libs/LibField.jar')```

Após isso, no android studio, é só sincronizar(sync) as alterações e compilar o projeto android executando o build. Pronto, a lib já estará instalada no seu projeto.

[Link para baixar o JAR](https://gitlab.com/JucimarJunior/libfieldproject/-/blob/9c1e49bcfc511f10902f310f93de790b81e0fcc0/libs/LibField.jar)

## Uso
Agora que o JAR foi devidamente instalado, você poderá usar as funcionalidades da biblioteca no seu projeto. As anotações HTTPs são:

GET, PUT, PATCH, POST e DELETE

As respectivas chamadas para cada um dos verbos são: 
1. **getObject** para o GET: lista 1 ou mais objetos
2. **put** para o PUT: atualiza os dados dos objetos
3. **patch** para o PATCH
4. **post** para o POST: cria um novo objeto
5. **delete** para o DELETE: exclui um objeto indicando o respectivo "id"

Exemplo de uso do método **post** para criar um **objeto**:

~~~java
public class Main {

    static final Gson gson = new GsonBuilder().setPrettyPrinting().create();

    public static void requestHTTPs() throws IOException {

        LibField<Gibi> lf = new LibField<>(Gibi.class, "http://localhost:8080/meugibi/");

        Gibi gibi = new Gibi();
        gibi.setNome("Emergencias " + new Date());
        gibi.setTitulo("Médicas");
        gibi.setEditora("Colombia");
        gibi.setNumero("7878777877787878");

        lf.post(gibi, Gibi.class);

    }

    public static void main(String[] args) throws IOException, NoSuchFieldException, SecurityException {
        requestHTTPs();
    }
}
~~~

Agora a parte de links hateoas:

1. **getLinks()** para listar os links que serão utilizados

Exemplo de uso:

~~~java
public class Main {

    private static final Gson gson = new GsonBuilder().setPrettyPrinting().create();

    private static void post() throws IOException {
        LibField<Gibi> lf = new LibField<>(Gibi.class, "http://localhost:8080/meugibi/1");

        Gibi gibi = new Gibi();

        System.out.println("Gibi: " + gson.toJson(lf.getObject(gibi, Gibi.class).getLinks()));
    }
}
~~~

Esta anotação lista somente os links dos objetos existentes em uma API

## Pré-requisitos
Para que tudo funcione bem, siga estes passos:

1. Recomendação de IDE: Android Studio ou Intelij
2. Versão do Java (JDK): 17.0.11
3. A API REST que você for usar para integrar em seu aplicativo, deve ter conteúdo HATEOAS e estar no formato HAL para funcionar como o esperado. Se estiver em outro formato como por exemplo, RPC (Remote Procedure Call) ou qualquer outro que não siga as restrições propostas por Roy Fielding, não irá funcionar corretamente e poderá apresentar erros.
4. De preferência, seu projeto deve estar configurado (bluid system) para receber dados no gradle.

## License

MIT License

Copyright (c) 2024 Jucimar Junior

## Status do Projeto
Concluído ✔️